#include "Cipher.hpp"

#include "Ceasar.hpp"
#include "Vigenere.hpp"

#include <shared/Config.hpp>
#include <shared/ExecutionCodes.hpp>

#include <cstring>

//! @version 1.0.0
Cipher::Cipher(uint8 cipherType) : cipherType(cipherType)
{}

//! @version 1.0.0
uint8 Cipher::GetCipherType() const
{
    return cipherType;
}

//! @version 1.1.0
Cipher *Cipher::ConstructCipher(Config *c, uint8 &errorCode)
{
#ifdef EXTENDED_CHECKS
    ASSERT(c != NULL, "CeasarCipher::Construct - no config provided");
    ASSERT(errorCode == 0, "CeasarCipher::Construct - entry error code is not 0");
#endif

    ccstring cipherStr = c->GetConfigString(CONFIG_FIELD_CIPHER);
    Cipher* res = NULL;

    if(cipherStr)
    {
        if(!strcmp("ceasar", cipherStr) || !strcmp("ceas", cipherStr))
            return CeasarCipher::Construct(c, errorCode);
        if(!strcmp("vigenere", cipherStr) || !strcmp("vig", cipherStr))
            return VigenereCipher::Construct(c, errorCode);
        else
        {
            errorCode = ECODE_INVALID_CIPHER;
            printf("Invalid cipher!\n");
        }
    }
    else
    {
        errorCode = ECODE_NOT_ENOUGH_PARAMETERS;
        printf("Specify a cipher to use using -cipher <cipher_type>\n");
    }

    return res;
}
