#ifndef CEASAR_H
#define CEASAR_H

#include "Cipher.hpp"

//! @brief Cipher for encrypting data using Ceasar Cipher
//! @details Takes base letter or number as a base.
//! @author Monich
//! @version 1.1.0
class CeasarCipher : public Cipher
{
public:
    //! @brief Constructor of Ceasar cipher using letter.
    //! @details Translates letter to numerical base.
    //! @param key - letter used as a key.
    explicit CeasarCipher(char key);
    //! @brief Constructor of Ceasar cipher using number.
    //! @param key - cryptographic key.
    explicit CeasarCipher(int8 numkey);

    bool Encode(std::istream &input, std::ostream OUTARG &output) override;

    bool Decode(std::istream &input, std::ostream OUTARG &output) override;

    //! @brief Returns numerical base for this cipher.
    //! @details Numerical base is a decoded cryptographic key for Ceasar cipher.
    //! @returns numerical base for this cipher.
    int8 GetNumKey() const;

    //! @brief Returns new CeasarCipher using data in Config.'
    //! @returns new CeasarCipher pointer. NULL if error was produced.
    //! @param c - config holding information about the cipher. You don't need to check if it contains proper information.
    //! @param errorCode - returns error code produced. Always 0 if return value wasn't 0.
    static CeasarCipher* TODELETE NULLABLE Construct(Config* c, uint8& OUTARG errorCode);

private:
    //! @brief Key used as a base in cipher.
    int8 numkey;

    //! @brief Returns encoded character based on single character.
    //! @returns encoded char.
    //! @param c - character to encode.
    char _EncodeChar(char c);
    //! @brief Returns decoded character based on single character.
    //! @returns decoded char.
    //! @param c - character to decode.
    char _DecodeChar(char c);
};

#endif // !CEASAR_H
