#ifndef VIGENERE_H
#define VIGENERE_H

#include "Cipher.hpp"

#include <queue>

class CeasarCipher;

//! @brief Cipher for encrypting data using Vigenere cipher
//! @details Vigenere cipher encrypts data using several Ceasar ciphers with different key values.
//! @author Monich
//! @version 1.0.0
class VigenereCipher : public Cipher
{
public:
    //! @brief Constructor of Vigenere cipher using letter.
    //! @details Translates letters to numerical base in the right sequence.
    //! @param key - string used as a key.
    explicit VigenereCipher(ccstring key);

    bool Encode(std::istream &input, std::ostream OUTARG &output) override;

    bool Decode(std::istream &input, std::ostream OUTARG &output) override;

    //! @brief Returns new VigenereCipher using data in Config.
    //! @returns new VigenereCipher pointer. NULL if error was produced.
    //! @param c - config holding information about the cipher. You don't need to check if it contains proper information.
    //! @param errorCode - returns error code produced. Always 0 if return value wasn't 0.
    static VigenereCipher* TODELETE NULLABLE Construct(Config* c, uint8& OUTARG errorCode);

private:
    //! @brief Sequence of chars used in encrypting.
    //! @details Used as a rotation queue, every used object is added to the end of the queue.
    std::queue<int8> cipherSequence;

    //! @brief Encodes character provided using number saved in queue.
    //! @post If encoding character in key queue was used, rotates the queue.
    //! @returns encoded char.
    //! @param c - character to encode.
    char _EncodeChar(char c);
    //! @brief Decodes character provided using number saved in queue.
    //! @post If encoding character in key queue was used, rotates the queue.
    //! @returns decoded char.
    //! @param c - character to decode.
    char _DecodeChar(char c);

    //! @brief Rotates the queue.
    //! @details Pops the first object used to encode and pushes it to the end.
    void _SkipKey();
};

#endif // !VIGENERE_H
