#include "Vigenere.hpp"

#include <shared/Config.hpp>
#include <shared/ExecutionCodes.hpp>

#include <cstring>
#include <string>

//! @version 1.0.0
VigenereCipher::VigenereCipher(ccstring key) : Cipher(CIPHER_VIGENERE)
{
#ifdef EXTENDED_CHECKS
    ASSERT(key != NULL, "VigenereCipher - no key provided");
    ASSERT(strlen(key) != 0, "VigenereCipher - empty key provided");
    const char* c = key;

    do
        ASSERT(isalpha(*c) != 0, "VigenereCipher - key contains non alphabetic characters");
    while(*(++c));
#endif

    const char* ch = key;

    do
    {
        char k = *ch;
        int8 numkey;
        if (k > 'Z')
            numkey = k - 'a';
        else numkey = k - 'A';
        cipherSequence.push(numkey);
    }while(*(++ch));
}

//! @version 1.0.0
bool VigenereCipher::Encode(std::istream &input, std::ostream &output)
{
    std::string in;
    cstring out;

    while(std::getline(input, in))
    {
        out = prepareCString(in.size());
        for (int i = 0; i < in.size(); ++i)
            out[i] = _EncodeChar(in[i]);
        output << out;
        delete[] out;
    }

    return true;
}

//! @version 1.0.0
bool VigenereCipher::Decode(std::istream &input, std::ostream &output)
{
    std::string in;
    cstring out;

    while(std::getline(input, in))
    {
        out = prepareCString(in.size());
        for (int i = 0; i < in.size(); ++i)
            out[i] = _DecodeChar(in[i]);
        output << out;
        delete[] out;
    }

    return true;
}

//! @version 1.0.0
VigenereCipher *VigenereCipher::Construct(Config *c, uint8 &errorCode)
{
#ifdef EXTENDED_CHECKS
    ASSERT(c != NULL, "CeasarCipher::Construct - no config provided");
    ASSERT(errorCode == 0, "CeasarCipher::Construct - entry error code is not 0");
#endif

    ccstring key = c->GetConfigString(CONFIG_FIELD_KEY);
    if(!key)
    {
        errorCode = ECODE_NOT_ENOUGH_PARAMETERS;
        printf("Vigenere cipher - no key parameter\n");
        return NULL;
    }

    if(strlen(key) == 0)
    {
        errorCode = ECODE_PARAMETER_DATA_INVALID;
        printf("Vigenere cipher - key must be a word\n");
        return NULL;
    }

    const char* ch = key;
    do
        if(!isalpha(*ch))
        {
            errorCode = ECODE_PARAMETER_DATA_INVALID;
            printf("Vigenere cipher - key must be a word\n");
            return NULL;
        }
    while(*(++ch));

    return new VigenereCipher(key);
}

//! @version 1.0.0
char VigenereCipher::_EncodeChar(char c)
{
#ifdef EXTENDED_CHECKS
    ASSERT(!cipherSequence.empty(), "VigenereCipher::_EncodeChar - cipher sequence is empty");
#endif
    bool skip = true;
    int8 numkey = cipherSequence.front();

    if(isupper(c))
        c = (c - 'A' + numkey) % char(26) + 'A';
    else if(islower(c))
        c = (c - 'a' + numkey) % char(26) + 'a';
    else skip = false;

    if(skip)
        _SkipKey();

    return c;
}

//! @version 1.0.0
char VigenereCipher::_DecodeChar(char c)
{
#ifdef EXTENDED_CHECKS
    ASSERT(!cipherSequence.empty(), "VigenereCipher::_DecodeChar - cipher sequence is empty");
#endif
    bool skip = true;
    int8 numkey = cipherSequence.front();

    if(isupper(c))
        c = (char(26) + c - 'A' - numkey) % char(26) + 'A';
    else if(islower(c))
        c = (char(26) + c - 'a' - numkey ) % char(26) + 'a';
    else skip = false;

    if(skip)
        _SkipKey();

    return c;
}

//! @version 1.0.0
void VigenereCipher::_SkipKey()
{
#ifdef EXTENDED_CHECKS
    ASSERT(!cipherSequence.empty(), "VigenereCipher::_SkipKey - cipher sequence is empty");
#endif
    int8 used = cipherSequence.front();
    cipherSequence.pop();
    cipherSequence.push(used);
}
