#include "Ceasar.hpp"

#include <shared/Config.hpp>
#include <shared/ExecutionCodes.hpp>

#include <cstring>
#include <string>

//! @version 1.0.0
CeasarCipher::CeasarCipher(char base) : Cipher(CIPHER_CEASAR)
{
#ifdef EXTENDED_CHECKS
    ASSERT((base >= 'A' && base <= 'Z') || (base >= 'a' && base <= 'z'), "CeasarCipher(char) - wrong base");
#endif

    if(base > 'Z')
        numkey = base - 'a';
    else numkey = base - 'A';
}

//! @version 1.0.0
CeasarCipher::CeasarCipher(int8 base) : Cipher(CIPHER_CEASAR), numkey(base)
{}

//! @version 1.0.0
bool CeasarCipher::Encode(std::istream &input, std::ostream OUTARG &output)
{
    std::string in;
    cstring out;

    while(std::getline(input, in))
    {
        out = prepareCString(in.size());
        for (int i = 0; i < in.size(); ++i)
            out[i] = _EncodeChar(in[i]);
        output << out;
        delete[] out;
    }

    return true;
}

//! @version 1.0.0
bool CeasarCipher::Decode(std::istream &input, std::ostream OUTARG &output)
{
    std::string in;
    cstring out;

    while(std::getline(input, in))
    {
        out = prepareCString(in.size());
        for (int i = 0; i < in.size(); ++i)
            out[i] = _DecodeChar(in[i]);
        output << out;
        delete[] out;
    }

    return true;
}

//! @version 1.0.0
int8 CeasarCipher::GetNumKey() const
{
    return numkey;
}

//! @version 1.0.1
CeasarCipher *CeasarCipher::Construct(Config *c, uint8 OUTARG &errorCode)
{
#ifdef EXTENDED_CHECKS
    ASSERT(c != NULL, "CeasarCipher::Construct - no config provided");
    ASSERT(errorCode == 0, "CeasarCipher::Construct - entry error code is not 0");
#endif

    ccstring key = c->GetConfigString(CONFIG_FIELD_KEY);

    if(!key)
    {
        errorCode = ECODE_NOT_ENOUGH_PARAMETERS;
        printf("ceasar cipher - no key parameter\n");
        return NULL;
    }

    size_t  keyLength = strlen(key);

    if(keyLength == 1)
    {
        char k = key[0];
        if(isalpha(k))
            return new CeasarCipher(k);
    }

    long intkey = strtol(key, NULL, 10);
    if(intkey == 0 && key[0] != '0')
    {
        errorCode = ECODE_PARAMETER_DATA_INVALID;
        printf("%s is not a valid base for ceasar cipher. Use single letter or number.\n", key);
    }
    else if(intkey > 26 || intkey < -26)
    {
        errorCode = ECODE_PARAMETER_DATA_INVALID;
        printf("Numerical key is invalid, use value between 26 and -26");
    }
    else
    {
        int8 finalKey = static_cast<int8>(intkey);
        return new CeasarCipher(finalKey);
    }

    return NULL;
}

//! @version 1.0.0
char CeasarCipher::_EncodeChar(char c)
{
    if(isupper(c))
        c = (c - 'A' + numkey) % char(26) + 'A';
    else if(islower(c))
        c = (c - 'a' + numkey) % char(26) + 'a';
    return c;
}

//! @version 1.0.0
char CeasarCipher::_DecodeChar(char c)
{
    if(isupper(c))
        c = (char(26) + c - 'A' - numkey) % char(26) + 'A';
    else if(islower(c))
        c = (char(26) + c - 'a' - numkey ) % char(26) + 'a';
    return c;
}
