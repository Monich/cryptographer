#ifndef CIPHER_H
#define CIPHER_H

#include <istream>
#include <ostream>

#include <shared/helpers.hpp>

class Config;

//! @brief Identifiers of all ciphers in application.
//! @author Monich
//! @version 1.1.0
enum eCipherType
{
    CIPHER_CEASAR,
    CIPHER_VIGENERE,

    CIPHER_MAX
};

//! @brief Abstract class for all ciphers.
//! @details Has methods for encoding and decoding all inputs.
//! @author Monich
//! @version 1.2.0
class Cipher
{
public:
    virtual ~Cipher() = default;

    //! @brief Encodes given input and saves it in output.
    //! @returns true if operation is successful.
    //! @version 1.0.0
    virtual bool Encode(std::istream& input, std::ostream& OUTARG output) = 0;
    //! @brief Decodes given input and saves it in output.
    //! @returns true if operation is successful.
    //! @version 1.0.0
    virtual bool Decode(std::istream& input, std::ostream& OUTARG output) = 0;

    //! @brief Returns type of the cipher.
    //! @sa eCipherType
    //! @returns identifier of the type of this cipher.
    uint8 GetCipherType() const;

    //! @brief Returns new Cipher using data in Config.'
    //! @returns new Cipher pointer. NULL if error was produced.
    //! @param c - config holding information about the cipher. You don't need to check if it contains proper information.
    //! @param errorCode - returns error code produced. Always 0 if return value wasn't 0.
    static Cipher* TODELETE NULLABLE ConstructCipher(Config* c, uint8& errorCode);

protected:
    //! @brief Constructor for Cipher class.
    //! @param cipherType - id of this cipher from eCipherType.
    explicit Cipher(uint8 cipherType);

    //! @brief Identifier of this cipher's type.
    const uint8 cipherType;
};

#endif // !CIPHER_H
