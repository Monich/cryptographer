#include <shared/Interpreter.hpp>

#include <shared/ExecutionCodes.hpp>

#include <catch2/catch.hpp>

#include <experimental/filesystem>
#include <sstream>
#include <string>

namespace fs = std::experimental::filesystem;

#define EXECUTE(IN, EXRES) SECTION(IN){execute(IN, EXRES);}
#define EXECUTE_FILE(IN, EXRES, CONTENTS) SECTION(IN){execute_file(IN, EXRES, CONTENTS);}
#define EXECUTE_OUT(IN, EXRES) SECTION(IN){execute_out(IN, EXRES);}

void execute(ccstring input, int expectedResult)
{
    std::vector<ccstring> split;
    std::string temp;

    for (std::stringstream ss(input); ss >> temp;)
        split.push_back(copyCString(temp.c_str()));

    split.push_back(NULL);

    int res = Interpreter::Launch(static_cast<int>(split.size()) - 1, split.data());
    REQUIRE(res == expectedResult);

    for (int i = 0; i < split.size(); ++i)
        delete[] split[i];
}

void execute_file(ccstring input, int expectedResult, ccstring contents)
{
    fs::path path(fs::temp_directory_path());
    path.append("cartographer.test");

#if _MSC_VER 
	size_t lenght = wcslen(path.c_str());
	cstring filename = prepareCString(lenght);
	wcstombs(filename, path.c_str(), lenght);
	FILE* file = fopen(filename, "w");
#else 
	FILE* file = fopen(path.c_str(), "w");
#endif 

    REQUIRE(file);
    fprintf(file, "%s", contents);
    fclose(file);

#if _MSC_VER
	cstring inputWithFile = mergeCString(input, filename);
	delete[] filename;
#else
	cstring inputWithFile = mergeCString(input, path.c_str());
#endif

    execute(inputWithFile, expectedResult);
    delete[] inputWithFile;

    fs::remove(path);
}

void execute_out(ccstring input, int expectedResult)
{
    fs::path path(fs::temp_directory_path());
    path.append("cartographer.out.test");

#if _MSC_VER
	size_t lenght = wcslen(path.c_str());
	cstring filename = prepareCString(lenght);
	wcstombs(filename, path.c_str(), lenght);
	cstring inputWithFile = mergeCString(input, filename);
	delete[] filename;
#else
	cstring inputWithFile = mergeCString(input, path.c_str());
#endif

    execute(inputWithFile, expectedResult);
    delete[] inputWithFile;

    fs::remove(path);
}

TEST_CASE("shared/Interpreter", "[shared][Interpreter]")
{
    // Checking for invalid inputs
    EXECUTE("cartographer", ECODE_NO_COMMAND)
    EXECUTE("cartographer xxx", ECODE_COMMAND_UNKNOWN)
    EXECUTE("cartographer help -no", ECODE_COMMAND_UNKNOWN)
    EXECUTE("cartographer encode -some_weird_argument", ECODE_PARAMETER_UNKNOWN)
    EXECUTE("cartographer decode -in", ECODE_NO_PARAMETER_DATA)

    // Checking for help
    EXECUTE("cartorapher help", ECODE_SUCCESS)
    EXECUTE("cartographer help encode", ECODE_SUCCESS)

    // Checking for Ciphers
    EXECUTE("cartographer encode", ECODE_NOT_ENOUGH_PARAMETERS);
    EXECUTE("cartographer decode -in word", ECODE_NOT_ENOUGH_PARAMETERS);
    EXECUTE("cartographer encode -cipher ceasar -in word", ECODE_NOT_ENOUGH_PARAMETERS);
    EXECUTE("cartographer decode -in word -cipher ceasar -key K", ECODE_SUCCESS);
    EXECUTE("cartographer encode -in word -cipher ceas -key 12", ECODE_SUCCESS);
    EXECUTE("cartographer decode -in word -cipher something_weird", ECODE_INVALID_CIPHER);

    // Checking for file input
    EXECUTE_FILE("cartographer encode  -cipher ceasar -key Z -file ", ECODE_SUCCESS, "Its a sentence");
    EXECUTE("cartographer decode -file cartographer.test -cipher ceasar -key U", ECODE_FILE_UNKNOWN);
    EXECUTE_FILE("cartographer encode -cipher ceasar -key Q -in word -file ", ECODE_PARAMETER_DATA_INVALID, "Its a sentence");

    // Checking for file output
    EXECUTE_OUT("cartographer encode -in word -cipher ceas -key 12 -out ", ECODE_SUCCESS);

    // Checking for sentence input
    EXECUTE("cartographer encode -in \"Gotcha\" -cipher ceas -key 0", ECODE_SUCCESS);
    EXECUTE("cartographer encode -in \"Short sentence\" -cipher ceas -key 0", ECODE_SUCCESS);
    EXECUTE("cartographer encode -in \"This is a sentence\" -cipher ceas -key 0", ECODE_SUCCESS);
    EXECUTE("cartographer encode -in \"This is a sentence -cipher ceas -key 0", ECODE_PARAMETER_DATA_INVALID);

    // Checking for Vigenere cipher and its keys
    EXECUTE("cartographer encode -in word -key th -cipher vigenere", ECODE_SUCCESS);
    EXECUTE("cartographer encode -in word -key th -cipher vig", ECODE_SUCCESS);
    EXECUTE("cartographer encode -in word -key \"y p\" -cipher vig", ECODE_PARAMETER_DATA_INVALID);
    EXECUTE("cartographer encode -in word -cipher vig", ECODE_NOT_ENOUGH_PARAMETERS);
    EXECUTE("cartographer encode -in word -key thisisa&key -cipher vig", ECODE_PARAMETER_DATA_INVALID);
}