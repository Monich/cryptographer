#include <shared/helpers.hpp>

#include <catch2/catch.hpp>

#include <cstring>
#include <string>

TEST_CASE("shared/helpers/prepareCString", "[shared][helpers]")
{
    cstring str;
    str = prepareCString(1);
    REQUIRE(str);

    str[0] = 'a';

    REQUIRE(strlen(str) == 1);
    REQUIRE(!strcmp("a", str));

    delete[] str;
}

TEST_CASE("shared/helpers/copyCString", "[shared][helpers]")
{
    ccstring origString = NULL;

    SECTION("empty string")
    {
        origString = "";
    }
    SECTION("short string")
    {
        origString = "test string";
    }
    SECTION("lorem ipsum")
    {
        origString = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce hendrerit neque a massa lacinia, sed elementum turpis sollicitudin. Donec iaculis efficitur lorem a cursus. Curabitur euismod varius nibh. Suspendisse tempor porttitor finibus. Donec iaculis tincidunt nunc, sed bibendum ante tincidunt vitae. Donec mauris velit, lobortis a mauris id, accumsan tristique enim. Praesent pharetra at mauris ut bibendum. Aenean imperdiet, justo quis placerat feugiat, est ligula ornare velit, sed rutrum massa ligula non erat. Cras convallis sollicitudin diam, eu efficitur lacus luctus sit amet. Integer feugiat lorem vitae nibh malesuada, eu pellentesque ante convallis.";
    }

    ccstring newString = copyCString(origString);

    REQUIRE(!strcmp(newString, origString));

    delete[] newString;
}

TEST_CASE("shared/helpers/mergeCString2", "[shared][helpers]")
{
    ccstring str1 = NULL, str2 = NULL;

    SECTION("empty string")
    {
        str1 = str2 = "";
    }
    SECTION("normal strings")
    {
        str1 = "lorem ";
        str2 = "ipsum\n";
    }

    std::string mergedStr(str1);
    mergedStr.append(str2);

    ccstring resStr = mergeCString(str1, str2);
    REQUIRE(resStr);
    REQUIRE(!strcmp(mergedStr.c_str(), resStr));

    delete[] resStr;
}

TEST_CASE("shared/helpers/mergeCString3", "[shared][helpers]")
{
    ccstring str1 = NULL, str2 = NULL, str3 = NULL;

    SECTION("empty string")
    {
        str1 = str2 = str3 = "";
    }
    SECTION("normal strings")
    {
        str1 = "lorem";
        str2 = " ";
        str3 = "ipsum\n";
    }

    std::string mergedStr(str1);
    mergedStr.append(str2);
    mergedStr.append(str3);

    ccstring resStr = mergeCString(str1, str2, str3);
    REQUIRE(resStr);
    REQUIRE(!strcmp(mergedStr.c_str(), resStr));

    delete[] resStr;
}
