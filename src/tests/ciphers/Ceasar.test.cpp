#include <ciphers/Ceasar.cpp>

#include <catch2/catch.hpp>

#include <cstring>
#include <experimental/filesystem>
#include <sstream>
#include <fstream>

namespace fs = std::experimental::filesystem;

template<typename T>
void launchTest(ccstring input, ccstring expectedOutput, T base)
{
    CeasarCipher cipher(base);

    { // Encoding
        std::istringstream in(input);
        std::ostringstream out;

        cipher.Encode(in, out);

        std::string str = out.str();
        ccstring output = str.c_str();

        REQUIRE(!strcmp(output, expectedOutput));
    }

    { // Decoding
        std::istringstream in(expectedOutput);
        std::ostringstream out;

        cipher.Decode(in, out);

        std::string str = out.str();
        ccstring output = str.c_str();

        REQUIRE(!strcmp(output, input));
    }

    { // File encoding
        fs::path path(fs::temp_directory_path());
        path.append("crypthographer.test");

#if _MSC_VER
		size_t lenght = wcslen(path.c_str());
		cstring filename = prepareCString(lenght);
		wcstombs(filename, path.c_str(), lenght);
		FILE* file = fopen(filename, "w");
#else
		FILE* file = fopen(path.c_str(), "w");
#endif
		
        REQUIRE(file);
        fprintf(file, "%s", input);
        fclose(file);

        std::ifstream in(path);
        std::ostringstream out;

        REQUIRE(in.good());
        cipher.Encode(in, out);

        std::string str = out.str();
        ccstring output = str.c_str();

        REQUIRE(!strcmp(output, expectedOutput));
    }

    { // To file encoding
        fs::path path(fs::temp_directory_path());
        path.append("crypthographer.out.test");

        std::istringstream in(input);
        std::ofstream out(path);

        REQUIRE(out.good());
        cipher.Encode(in, out);
        out.close();

#if _MSC_VER
        size_t lenght = wcslen(path.c_str());
		cstring filename = prepareCString(lenght);
		wcstombs(filename, path.c_str(), lenght);
		FILE* file = fopen(filename, "r");
#else
        FILE* file = fopen(path.c_str(), "r");
#endif

        REQUIRE(file);
        fseek(file, 0, SEEK_END);
        long fsize = ftell(file);
        fseek(file, 0, SEEK_SET);

        cstring output = prepareCString(fsize);
        fread(output, fsize, 1, file);
        fclose(file);

        REQUIRE(!strcmp(output, expectedOutput));
        delete[] output;
    }
}

TEST_CASE("ciphers/Ceasar", "[ciphers][Ceasar]")
{
    SECTION("empty string")
    {
        launchTest("", "", 'c');
        launchTest("", "", ((int8)13));
    }

    SECTION("nulled key")
    {
        launchTest("something", "something", 'a');
        launchTest("other", "other", ((int8)0));
    }

    SECTION("only add strings")
    {
        launchTest("abc", "bcd", 'b');
        launchTest("def", "fgh", ((int8)2));
    }

    SECTION("only subtract strings")
    {
        launchTest("xyz", "abc", 'd');
        launchTest("zyx", "fed", ((int8)6));
    }

    SECTION("mixed strings")
    {
        launchTest("This is a sentence", "Mabl bl t lxgmxgvx", 'T');
        launchTest("Testing in progress", "Docdsxq sx zbyqbocc", ((int8)10));
    }

    SECTION("special characters")
    {
        launchTest("8Q/21]|^za0t\t", "8D/21]|^mn0g\t", 'N');
        launchTest("h@ll0-w0r...''", "e@ii0-t0o...''", ((int8)23));
    }
}
