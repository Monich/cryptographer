#include <ciphers/Cipher.hpp>

#include <catch2/catch.hpp>

#include <ciphers/Ceasar.hpp>
#include <shared/Config.hpp>
#include <shared/ExecutionCodes.hpp>

Cipher* execute(uint8 expectedCipherId, uint8 expectedErrorCode, Config& c)
{
    uint8 errorCode = 0;
    Cipher* cipher = Cipher::ConstructCipher(&c, errorCode);

    REQUIRE(errorCode == expectedErrorCode);

    if(expectedCipherId == CIPHER_MAX)
        REQUIRE(!cipher);
    else
    {
        REQUIRE(cipher);
        REQUIRE(cipher->GetCipherType() == expectedCipherId);
    }

    return cipher;
}

TEST_CASE("ciphers/Cipher/ConstructCipher", "[ciphers][Cipher]")
{
    Config c;

    SECTION("no cipher specified")
    {
        execute(CIPHER_MAX, ECODE_NOT_ENOUGH_PARAMETERS, c);
    }

    SECTION("-cipher xd")
    {
        c.SetConfig(CONFIG_FIELD_CIPHER, "xd");
        execute(CIPHER_MAX, ECODE_INVALID_CIPHER, c);
    }

    SECTION("-cipher ceasar")
    {
        c.SetConfig(CONFIG_FIELD_CIPHER, "ceasar");
        execute(CIPHER_MAX, ECODE_NOT_ENOUGH_PARAMETERS, c);

        c.SetConfig(CONFIG_FIELD_CIPHER, "ceas");
        execute(CIPHER_MAX, ECODE_NOT_ENOUGH_PARAMETERS, c);
    }

    SECTION("-cipher ceasar -key K")
    {
        c.SetConfig(CONFIG_FIELD_CIPHER, "ceasar");
        c.SetConfig(CONFIG_FIELD_KEY, "k");
        Cipher* cipher = execute(CIPHER_CEASAR, ECODE_SUCCESS, c);
        REQUIRE(dynamic_cast<CeasarCipher*>(cipher)->GetNumKey() == 'k' - 'a');
        delete cipher;

        c.SetConfig(CONFIG_FIELD_CIPHER, "ceasar");
        c.SetConfig(CONFIG_FIELD_KEY, "K");
        cipher = execute(CIPHER_CEASAR, ECODE_SUCCESS, c);
        REQUIRE(dynamic_cast<CeasarCipher*>(cipher)->GetNumKey() == 'k' - 'a');
        delete cipher;
    }

    SECTION("-cipher ceasar -key 12")
    {
        c.SetConfig(CONFIG_FIELD_CIPHER, "ceas");
        c.SetConfig(CONFIG_FIELD_KEY, "12");
        Cipher* cipher = execute(CIPHER_CEASAR, ECODE_SUCCESS, c);
        REQUIRE(dynamic_cast<CeasarCipher*>(cipher)->GetNumKey() == 12);
        delete cipher;

        c.SetConfig(CONFIG_FIELD_CIPHER, "ceas");
        c.SetConfig(CONFIG_FIELD_KEY, "12");
        cipher = execute(CIPHER_CEASAR, ECODE_SUCCESS, c);
        REQUIRE(dynamic_cast<CeasarCipher*>(cipher)->GetNumKey() == 12);
        delete cipher;
    }

    SECTION("-cipher ceasar -key -")
    {
        c.SetConfig(CONFIG_FIELD_CIPHER, "ceasar");
        c.SetConfig(CONFIG_FIELD_KEY, "-");
        execute(CIPHER_MAX, ECODE_PARAMETER_DATA_INVALID, c);
    }

    SECTION("-cipher ceasar -key 100")
    {
        c.SetConfig(CONFIG_FIELD_CIPHER, "ceasar");
        c.SetConfig(CONFIG_FIELD_KEY, "100");
        execute(CIPHER_MAX, ECODE_PARAMETER_DATA_INVALID, c);
    }

    SECTION("-cipher ceas -key abc")
    {
        c.SetConfig(CONFIG_FIELD_CIPHER, "ceas");
        c.SetConfig(CONFIG_FIELD_KEY, "abc");
        execute(CIPHER_MAX, ECODE_PARAMETER_DATA_INVALID, c);
    }
}