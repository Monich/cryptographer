#include <ciphers/Vigenere.hpp>

#include <catch2/catch.hpp>

#include <cstring>
#include <experimental/filesystem>
#include <sstream>
#include <fstream>

namespace fs = std::experimental::filesystem;

void launchTest(ccstring input, ccstring expectedOutput, ccstring base)
{
    { // Encoding
        VigenereCipher cipher(base);

        std::istringstream in(input);
        std::ostringstream out;

        cipher.Encode(in, out);

        std::string str = out.str();
        ccstring output = str.c_str();

        REQUIRE(!strcmp(output, expectedOutput));
    }

    { // Decoding
        VigenereCipher cipher(base);

        std::istringstream in(expectedOutput);
        std::ostringstream out;

        cipher.Decode(in, out);

        std::string str = out.str();
        ccstring output = str.c_str();

        REQUIRE(!strcmp(output, input));
    }

    { // File encoding
        VigenereCipher cipher(base);

        fs::path path(fs::temp_directory_path());
        path.append("crypthographer.test");

#if _MSC_VER
		size_t lenght = wcslen(path.c_str());
		cstring filename = prepareCString(lenght);
		wcstombs(filename, path.c_str(), lenght);
		FILE* file = fopen(filename, "w");
#else
		FILE* file = fopen(path.c_str(), "w");
#endif
		
        REQUIRE(file);
        fprintf(file, "%s", input);
        fclose(file);

        std::ifstream in(path);
        std::ostringstream out;

        REQUIRE(in.good());
        cipher.Encode(in, out);

        std::string str = out.str();
        ccstring output = str.c_str();

        REQUIRE(!strcmp(output, expectedOutput));
    }

    { // To file encoding
        VigenereCipher cipher(base);

        fs::path path(fs::temp_directory_path());
        path.append("crypthographer.out.test");

        std::istringstream in(input);
        std::ofstream out(path);

        REQUIRE(out.good());
        cipher.Encode(in, out);
        out.close();

#if _MSC_VER
        size_t lenght = wcslen(path.c_str());
		cstring filename = prepareCString(lenght);
		wcstombs(filename, path.c_str(), lenght);
		FILE* file = fopen(filename, "r");
#else
        FILE* file = fopen(path.c_str(), "r");
#endif

        REQUIRE(file);
        fseek(file, 0, SEEK_END);
        long fsize = ftell(file);
        fseek(file, 0, SEEK_SET);

        cstring output = prepareCString(fsize);
        fread(output, fsize, 1, file);
        fclose(file);

        REQUIRE(!strcmp(output, expectedOutput));
        delete[] output;
    }
}

TEST_CASE("ciphers/Vigenere", "[ciphers][Vigenere]")
{
    SECTION("empty string")
    {
        launchTest("", "", "key");
    }

    SECTION("nulled key")
    {
        launchTest("something", "something", "a");
    }

    SECTION("key lenght is word lenght")
    {
        launchTest("string", "gmymey", "others");
    }

    SECTION("key lenght is longer than word lenght")
    {
        launchTest("this", "evvy", "longer");
    }

    SECTION("key lenght is shorter than word lenght")
    {
        launchTest("thisshouldbeforgotten", "ewcdhbzjfoqyqdlrdneth", "lpu");
    }

    SECTION("string")
    {
        launchTest("This is a sentence", "Dlgc mq k wcxxcxgc", "key");
    }

    SECTION("special characters")
    {
        launchTest("8Q/21]|^za0t\t", "8A/21]|^dy0d\t", "KeY");
    }
}
