#include <shared/Interpreter.hpp>

int main(int argc, ccstring* argv)
{
    return Interpreter::Launch(argc, argv);
}
