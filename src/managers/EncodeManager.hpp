#ifndef ENCODE_MANAGER_H
#define ENCODE_MANAGER_H

#include <shared/helpers.hpp>

#include <istream>

class Cipher;
class Config;

//! @brief Manager executing encoding and decoding using ciphers.
//! @author Monich
//! @version 1.0.1
class EncodeManager
{
public:
    //! @brief Launches encoding operation.
    //! @return ecode for this operation.
    static int Launch(int argc, ccstring* argv, Config* c);
    //! @brief Launches decoding operation.
    //! @return ecode for this operation.
    //! @param c - Config of this operation.
    static int LaunchDecode(int argc, ccstring* argv, Config* c);

    //! @brief Prints help for encoding and decoding on screen.
    static void LaunchHelp();

private:
    //! @brief Prepares application for using ciphers.
    //! @details Checks if all needed config parameters are set and generates a Cipher.
    //! @returns ecode of this operation.
    //! @param c - Config of this operation.
    //! @param cipherPtr - newly generated cipher is saved in this location.
    static int _Check(Config* c, Cipher** OUTARG cipherPtr);

    //! @brief Generates an input stream to load from.
    //! @details Depending on the input requested by invoker, it may open file or prepare a string stream to read from.
    //! @returns ecode of this operation. Won't return success if the stream opening fails.
    //! @param c - Config of this operation.
    //! @param cipherInput - input stream for cipher to operate to.
    static int _GetInput(Config* c, std::istream** OUTARG cipherInput);

    //! @brief Generates an output stream to load for.
    //! @details Depending on the input requested by invoker, it may open file or pass console stream pointer.
    //! @returns ecode of this operation. Won't return success if the stream opening fails.
    //! @param c - Config of this operation.
    //! @param cipherOutput - output stream for cipher to operate to.
    static int _GetOutput(Config* c, std::ostream** OUTARG cipherOutput);
};

#endif // !ENCODE_MANAGER_H