#include "EncodeManager.hpp"

#include <ciphers/Cipher.hpp>
#include <shared/Config.hpp>
#include <shared/ExecutionCodes.hpp>

#include <fstream>
#include <iostream>
#include <sstream>

//! @version 1.2.0
int EncodeManager::Launch(int argc, ccstring *argv, Config *c)
{
#ifdef EXTENDED_CHECKS
    ASSERT(c != NULL, "EncodeManager::Launch - no config provided");
#endif

    Cipher* cipher = NULL;
    int ecode = _Check(c, &cipher);
    if(ecode)
        return ecode;

    std::istream* cipherInput = NULL;
    ecode = _GetInput(c, &cipherInput);
    if(ecode)
    {
        delete cipher;
        return ecode;
    }

    std::ostream* cipherOutput = NULL;
    ecode = _GetOutput(c, &cipherOutput);
    if(ecode)
    {
        delete cipherInput;
        delete cipher;
        return ecode;
    }

    cipher->Encode(*cipherInput, *cipherOutput);
    printf("\n");

    delete cipher;
    delete cipherInput;
    if(cipherOutput != &std::cout)
        delete cipherOutput;
    return ECODE_SUCCESS;
}

//! @version 1.2.0
int EncodeManager::LaunchDecode(int argc, ccstring *argv, Config *c)
{
#ifdef EXTENDED_CHECKS
    ASSERT(c != NULL, "EncodeManager::LaunchDecode - no config provided");
#endif

    Cipher* cipher = NULL;
    int ecode = _Check(c, &cipher);
    if(ecode)
        return ecode;

    std::istream* cipherInput = NULL;
    ecode = _GetInput(c, &cipherInput);
    if(ecode)
    {
        delete cipher;
        return ecode;
    }

    std::ostream* cipherOutput = NULL;
    ecode = _GetOutput(c, &cipherOutput);
    if(ecode)
    {
        delete cipherInput;
        delete cipher;
        return ecode;
    }

    cipher->Decode(*cipherInput, *cipherOutput);
    printf("\n");

    delete cipher;
    delete cipherInput;
    return ECODE_SUCCESS;
}

//! @version 1.2.0
void EncodeManager::LaunchHelp()
{
    printf("Encodes or decodes the cipher.\n"
           "Use -cipher to specify the cipher type\n"
           "Use -key to provide a key for a cipher\n"
           "Use -in to set input word or -file to set a set to encrypt\n"
           "Use -out to set a file to write to. If you don't, console will be used.\n");
}

//! @version 1.1.0
int EncodeManager::_Check(Config *c, Cipher **cipherPtr)
{
#ifdef EXTENDED_CHECKS
    ASSERT(c != NULL, "EncodeManager::_Check - no config provided");
    ASSERT(cipherPtr != NULL, "EncodeManager::_Check - no pointer for cipher");
#endif
    uint8 errorCode = 0;
    (*cipherPtr) = Cipher::ConstructCipher(c, errorCode);

    if(errorCode)
        return errorCode;

    return ECODE_SUCCESS;
}

//! @version 1.0.0
int EncodeManager::_GetInput(Config *c, std::istream **cipherInput)
{
#ifdef EXTENDED_CHECKS
    ASSERT(c != NULL, "EncodeManager::_GetInput - no config provided");
    ASSERT(cipherInput != NULL, "EncodeManager::_GetInput - no pointer for input stream");
#endif
    ccstring inWord, inFile;
    inWord = c->GetConfigString(CONFIG_FIELD_IN_WORD);
    inFile = c->GetConfigString(CONFIG_FIELD_IN_FILE);

    if(inWord && inFile)
    {
        printf("Parameters -file and -in used at the same time. Use only one of those.\n");
        return ECODE_PARAMETER_DATA_INVALID;
    }
    else if(inWord)
    {
        *cipherInput = new std::istringstream(inWord);
    }
    else if(inFile)
    {
        *cipherInput = new std::ifstream(inFile);
        if(!**cipherInput)
        {
            delete *cipherInput;
            printf("File %s couldn't be opened\n", inFile);
            return ECODE_FILE_UNKNOWN;
        }
    }
    else
    {
        printf("No input detected. Specify input using -in parameter.\n");
        return ECODE_NOT_ENOUGH_PARAMETERS;
    }

    return ECODE_SUCCESS;
}

//! @version 1.0.0
int EncodeManager::_GetOutput(Config *c, std::ostream **cipherOutput)
{
#ifdef EXTENDED_CHECKS
    ASSERT(c != NULL, "EncodeManager::_GetOuput - no config provided");
    ASSERT(cipherOutput != NULL, "EncodeManager::_GetOuput - no pointer for input stream");
#endif

    ccstring outFile = c->GetConfigString(CONFIG_FIELD_OUT_FILE);

    if(outFile)
    {
        *cipherOutput = new std::ofstream(outFile);
        if(!**cipherOutput)
        {
            delete *cipherOutput;
            printf("File %s couldn't be opened\n", outFile);
            return ECODE_FILE_UNKNOWN;
        }
    }
    else
    {
        *cipherOutput = &std::cout;
    }

    return ECODE_SUCCESS;
}
