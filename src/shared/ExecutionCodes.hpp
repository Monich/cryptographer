#ifndef EXECUTION_CODE_H
#define EXECUTION_CODE_H

//! @brief Execution codes that can be returned by application
//! @author Monich
//! @version 1.2.0
enum ExecutionCode
{
    ECODE_SUCCESS,
    ECODE_FAILURE,

    // Interpreter
    ECODE_NO_COMMAND,
    ECODE_COMMAND_UNKNOWN,
    ECODE_PARAMETER_UNKNOWN,
    ECODE_NO_PARAMETER_DATA,
    ECODE_FILE_UNKNOWN,

    // Managers
    ECODE_NOT_ENOUGH_PARAMETERS,
    ECODE_INVALID_CIPHER,
    ECODE_PARAMETER_DATA_INVALID,
};

#endif // !EXECUTION_CODE_H
