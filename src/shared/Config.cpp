#include "Config.hpp"
#include "ExecutionCodes.hpp"

#include <cstring>
#include <cstdlib>

//! @brief Enumeration of types of config fields.
//! @details Each config can be stored in two ways:
//!     It can retrieve some data from user or it can just be set whenever assosiated argument is used.
//!     For example: git push --delete <branch_name> vs git push --all.
//!     Type GET specifies the first behavior: Config class will try to retrieve some data after the argument is used.
//!     Type NEW will just log that the given argument was used and will move on with reading arguments.
//!     Type INVALID_KEY is returned is if key is invalid.
//! @version 1.0.0
//! @author Monich
enum ConfigFieldPropertiesType
{
	CONFIG_FIELD_PROPERTY_INVALID_KEY = 0,
	CONFIG_FIELD_PROPERTY_NEW,
	CONFIG_FIELD_PROPERTY_GET,
};

//! @brief Field for ConfigFieldPropertiesType enum.
typedef uint8 ConfigPropertiesType;

//! @brief Properties for all Config fields.
//! @details Defines rules for config reading. This structure shouldn't be created.
//! @version 1.0.0
//! @author Monich
struct ConfigFieldProperties
{
    //! @brief Key string for command line.
    //! @details If this string equals argument string from command line, the config field of given field id is being invoked.
	ccstring keyCmd;
    //! @brief Type of this config field.
    //! @sa enum ConfigFieldPropertiesType
    ConfigPropertiesType type;
    //! @brief Commands supported by this config.
    //! @details Contains flagged values of all CommandIds. Set 0 for turning off.
    CommandFlag commandsSupported;

    //! @brief Deleted default constructor.
    //! @warning This method is deleted.
	ConfigFieldProperties() = delete;

    //! @brief Returns type of config if key checks with key for command line.
    //! @returns config type if key is validated, CONFIG_FIELD_PROPERTY_INVALID_KEY if validation failed.
    //! @param key - key to be validated.
    ConfigPropertiesType GetTypeCmd(ccstring key) const;
    //! @brief Returns type of config if key checks with key for config file.
    //! @returns config type if key is validated, CONFIG_FIELD_PROPERTY_INVALID_KEY if validation failed.
    //! @param key - key to be validated.
    ConfigPropertiesType GetTypeFile(ccstring key) const;
    //! @brief Checks support of given config for given command.
    //! @returns true if command is supported.
    //! @param id - identifier of used command.
	bool Supports(CommandId id) const;
};

//! @brief Properties for all config files
//! @sa ConfigFieldProperties
//! @author Monich
//! @version 1.3.0
const ConfigFieldProperties fieldProperties[CONFIG_FIELD_MAX] =
{
	{    "-in", CONFIG_FIELD_PROPERTY_GET, 0}, // CONFIG_FIELD_IN_WORD
	{  "-file", CONFIG_FIELD_PROPERTY_GET, 0}, // CONFIG_FIELD_IN_FILE
	{   "-out", CONFIG_FIELD_PROPERTY_GET, 0}, // CONFIG_FIELD_OUT_FILE
	{"-cipher", CONFIG_FIELD_PROPERTY_GET, 0}, // CONFIG_FIELD_CIPHER
	{	"-key", CONFIG_FIELD_PROPERTY_GET, 0}, // CONFIG_FIELD_KEY
};

//! @version 1.0.0
Config::Config()
{
	configs = new cstring[CONFIG_FIELD_MAX];
	_SetDefaults();
}

//! @version 1.0.0
Config::~Config()
{
	for (int i = CONFIG_FIELD_MAX - 1; i >= 0; i--)
		if(configs[i])
		    delete[] configs[i];

	delete[] configs;
}

//! @version 1.0.0
const uint64 Config::GetConfigUnsigned(const ConfigField field) const
{
#ifdef EXTENDED_CHECKS
    ASSERT(field < CONFIG_FIELD_MAX, "Config::GetConfigUnsigned: Invalid config field");
#endif

    return strtoul(_GetConfig(field), NULL, 10);
}

//! @version 0.0.0
const int64 Config::GetConfigSigned(const ConfigField field) const
{
    throw "not implemented";
}

//! @version 0.0.0
const float64 Config::GetConfigFloat(const ConfigField field) const
{
    throw "not implemented";
}

//! @version 1.0.0
ccstring Config::GetConfigString(const ConfigField field) const
{
#ifdef EXTENDED_CHECKS
    ASSERT(field < CONFIG_FIELD_MAX, "Config::GetConfigString: Invalid config field");
#endif

    return _GetConfig(field);
}

//! @version 0.0.0
void Config::SetConfig(const ConfigField field, const uint64 value)
{
    throw "not implemented";
}

//! @version 0.0.0
void Config::SetConfig(const ConfigField field, const int64 value)
{
    throw "not implemented";
}

//! @version 1.0.2
void Config::SetConfig(const ConfigField field, const ccstring value)
{
#ifdef EXTENDED_CHECKS
    ASSERT(field < CONFIG_FIELD_MAX, "Config::SetConfig[ccstring]: Invalid config field");
    ASSERT(value != NULL, "Config:SetConfig[ccstring]: No value");
#endif

    _SetNewConfig(field, copyCString(value));
}

//! @version 1.0.0
void Config::SetConfig(const ConfigField field, const float64 value)
{
    throw "not implemented";
}

//! @version 1.0.2
bool Config::HasConfig(const ConfigField field) const
{
#ifdef EXTENDED_CHECKS
    ASSERT(field < CONFIG_FIELD_MAX, "Config::HasConfig: Invalid config field");
#endif
	return _GetConfig(field) != NULL;
}

//! @version 1.2.0
uint32 Config::RetrieveConfig(const int argc, ccstring * argv, const CommandId command)
{
#ifdef EXTENDED_CHECKS
    ASSERT(command && command < COMMAND_MAX, "Config::RetrieveConfig: No valid command");
#endif

	for (int j = 2; j < argc; j++)
	{
		ccstring argument = argv[j];
		bool argumentFound = false;

		for (ConfigField i = 0; i < CONFIG_FIELD_MAX; ++i)
			if (ConfigPropertiesType type = fieldProperties[i].GetTypeCmd(argument))
			{
				if (!fieldProperties[i].Supports(command))
					printf("%s - invalid argument for this command, will be omitted\n", argument);

				argumentFound = true;

				switch (type)
				{
				case CONFIG_FIELD_PROPERTY_NEW:
					SetConfig(i, "");
					break;
				case CONFIG_FIELD_PROPERTY_GET:
				{
					if (++j >= argc)
					{
						printf("%s - argument requires a value\n", argument);
						return ECODE_NO_PARAMETER_DATA;
					}
					if(argv[j][0] != '\"')
						SetConfig(i, argv[j]);
					else
					{
						cstring preparedArg = copyCString(&argv[j][1]);
						while(preparedArg[strlen(preparedArg)-1] != '\"')
						{
							if (++j >= argc)
							{
								printf("argument %s not finished with a \" sign\n", argument);
								delete[] preparedArg;
								return ECODE_PARAMETER_DATA_INVALID;
							}
							cstring temp = preparedArg;
							preparedArg = mergeCString(preparedArg, " ", argv[j]);
							delete[] temp;
						}
						preparedArg[strlen(preparedArg) - 1] = 0;
						_SetNewConfig(i, preparedArg);
					}
					break;
				}
				}

			}
		if (!argumentFound)
		{
            printf("%s - invalid argument\n", argument);
			return ECODE_PARAMETER_UNKNOWN;
		}
	}

	return ECODE_SUCCESS;
}

//! @version 1.0.1
ccstring Config::_GetConfig(const ConfigField field) const
{
#ifdef EXTENDED_CHECKS
    ASSERT(field < CONFIG_FIELD_MAX, "Config::_GetConfig: Invalid config field");
#endif

	return configs[field];
}

//! @version 1.0.0
void Config::_SetDefaults()
{
    _SetDefaultsNulled();
}

//! @version 1.0.0
void Config::_SetDefaultsNulled()
{
    for (int i = CONFIG_FIELD_MAX - 1; i >= 0; i--)
        configs[i] = NULL;
}

//! @version 1.0.2
void Config::_SetNewConfig(ConfigField field, cstring config)
{
#ifdef EXTENDED_CHECKS
    ASSERT(field < CONFIG_FIELD_MAX, "Config::_SetNewConfig: Invalid config field");
#endif

	delete[] _GetConfig(field);
	configs[field] = config;
}

//! @version 1.0.1
uint8 ConfigFieldProperties::GetTypeCmd(ccstring key) const
{
#ifdef EXTENDED_CHECKS
    ASSERT(key != NULL, "ConfigFieldProperties::GetTypeCmd: No key given");
#endif

	return strcmp(key, keyCmd) ? CONFIG_FIELD_PROPERTY_INVALID_KEY : type;
}

//! @version 1.0.0
bool ConfigFieldProperties::Supports(const CommandId id) const
{
	return !commandsSupported || (1 << id & commandsSupported);
}
