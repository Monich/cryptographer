#include "Interpreter.hpp"

#include "Config.hpp"
#include "ExecutionCodes.hpp"

#include <managers/EncodeManager.hpp>

#include <cstring>

#define INTERPRETER_COMMAND_COUNT 9

#define ENTRY_MESSAGE "Cryptographer by Monich ~2018\nType cryptographer -help for more information.\n"
#define HELP_MESSAGE "Help page for cryptographer\nType cryptographer --help <command> for more information\nAvailable commands:\n\thelp\n\tencode\n\tdecode\n"

//! @brief Class representing one of the possible methods to invoke a method from a command line.
//! @details This class shouldn't be allocated.
//! @author Monich
//! @version 1.0.0
struct InterpreterCommand
{
    //! @brief String key of the command.
    //! @details Represents argument read from command line.
    ccstring key;
    //! @brief Command to be used.
    //! @details Indentifier of the command associated with key.
    const CommandId command;

    //! @brief Deleted default constructor.
    //! @warning This method is deleted. Don't use it.
    InterpreterCommand() = delete;

    //! @brief Checks if given key read from command line matches the key of this instance.
    //! @returns true if string command matches with key.
    //! @param key - key to check against.
    const bool IsKey(ccstring key) const;
};

//! @brief Holds functions ponters for given command.
//! @author Monich
//! @version 1.0.0
struct InterpreterEntry
{
    //! @brief Main function of the command.
    const InterpreterFn func;
    //! @brief Help function of the command.
    const InterpreterHelpFn helpFunc;
};

//! @version 1.1.0
const InterpreterCommand iCommands[INTERPRETER_COMMAND_COUNT]
{
    { "help", COMMAND_HELP },
    { "-h", COMMAND_HELP },
    { "-help", COMMAND_HELP },
    { "--help", COMMAND_HELP },
    { "?", COMMAND_HELP },
    { "encode", COMMAND_ENCODE },
    { "en", COMMAND_ENCODE },
    { "decode", COMMAND_DECODE },
    { "de", COMMAND_DECODE },
};

//! @version 1.1.0
const InterpreterEntry iEntries[COMMAND_MAX] =
{
        { nullptr,                  nullptr},
        { Interpreter::LaunchHelp,  Interpreter::LaunchHelp_Help },
        { EncodeManager::Launch, EncodeManager::LaunchHelp },
        { EncodeManager::LaunchDecode, EncodeManager::LaunchHelp },
};

//! @version 1.0.0
int Interpreter::Launch(const int argc, ccstring* argv)
{
    ccstring command = argv[1];

    if (argc >= 1 && !command)
    {
        printf(ENTRY_MESSAGE);
        return ECODE_NO_COMMAND;
    }
    else
        if (auto id = _GetCommandIdByKey(command))
            return _PrepareCommand(id, argc, argv);

    printf("%s - no such command found\n", command);
    return ECODE_COMMAND_UNKNOWN;
}

//! @version 1.0.0
int Interpreter::LaunchHelp(const int argc, ccstring * argv, Config * c)
{
    if(argc > 2 && argv[2])
        if (auto entry = _GetCommandIdByKey(argv[2]))
            _GetInterpreterEntry(entry)->helpFunc();
        else
        {
            printf("%s - no such command found\n", argv[2]);
            return ECODE_COMMAND_UNKNOWN;
        }
    else
    {
        LaunchHelp_Help();
    }
    return ECODE_SUCCESS;
}

//! @version 1.1.0
void Interpreter::LaunchHelp_Help()
{
    printf(HELP_MESSAGE);
}

//! @version 2.0.0
int Interpreter::_PrepareCommand(const CommandId command, const int argc, ccstring * argv)
{
    Config c;

    if (command != COMMAND_HELP)
        if(uint32 code = c.RetrieveConfig(argc, argv, command))
            return code;

    return _GetInterpreterEntry(command)->func(argc, argv, &c);
}

//! @version 1.0.0
const CommandId Interpreter::_GetCommandIdByKey(ccstring key)
{
    for (int i = INTERPRETER_COMMAND_COUNT - 1; i >= 0; i--)
        if (iCommands[i].IsKey(key))
            return iCommands[i].command;
    return COMMAND_INVALID;
}

//! @version 1.0.0
const InterpreterEntry * Interpreter::_GetInterpreterEntry(const CommandId id)
{
    return &iEntries[id];
}

//! @version 1.0.0
const bool InterpreterCommand::IsKey(ccstring key) const
{
    return !strcmp(key, this->key);
}
