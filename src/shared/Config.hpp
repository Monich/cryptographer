#ifndef CONFIG_HPP
#define CONFIG_HPP

#include "Interpreter.hpp"

#include <cstdio>

//! @brief Enumeration of config fields that can be saved in the Config.
//! @deatails Each value represents available config field to be loaded.
//!         If given field is NULLABLE, the return value may not be stored in Config.
//! @version 1.3.0
enum ConfigFields
{
	CONFIG_FIELD_IN_WORD,
	CONFIG_FIELD_IN_FILE,
	CONFIG_FIELD_OUT_FILE,
	CONFIG_FIELD_CIPHER,
	CONFIG_FIELD_KEY,

    CONFIG_FIELD_MAX
};

typedef uint8 ConfigField;

//! @brief Configuration for the application.
//! @details Manages all command line parameters, loading them at startup and distributing it throughout the classes.
//! @author Monich
//! @version 1.0.1
class Config
{
public:
    //! @brief Default constructor
    //! @post Instance is created and initialized. Default values config fields are set.
	Config();

    //! @brief Default destructor.
    //! @post Instance is destroyed.
	~Config();

    //! @brief Get config value as unsigned integer.
    //! @returns unsigned value of the config field.
    //! @pre If parameter field is NULLABLE, check if Config stores value for that field using HasConfig(field).
    //! @param field - save field where the value is stored. See enum ConfigFields. Only values from this enum are supported.
	const uint64 GetConfigUnsigned(ConfigField field) const;
    //! @brief Get config value as signed integer.
    //! @returns signed value of the config field.
    //! @pre If parameter field is NULLABLE, check if Config stores value for that field using HasConfig(field).
    //! @param field - save field where the value is stored. See enum ConfigFields. Only values from this enum are supported.
    //! @warning This function is not yet implemented.
	const int64 GetConfigSigned(ConfigField field) const;
    //! @brief Get config value as floating point number.
    //! @returns floating point value of the config field.
    //! @pre If parameter field is NULLABLE, check if Config stores value for that field using HasConfig(field).
    //! @param field - save field where the value is stored. See enum ConfigFields. Only values from this enum are supported.
    //! @warning This function is not yet implemented.
	const float64 GetConfigFloat(ConfigField field) const;
    //! @brief Get config value as a string.
    //! @details Lose NULLABLE is field in ConfigFields is not NULLABLE.
    //! @returns value of the config field.
    //! @param field - save field where the value is stored. See enum ConfigFields. Only values from this enum are supported.
	ccstring NULLABLE GetConfigString(ConfigField field) const;

	//! @brief Sets given config file with unsigned value.
    //! @post Config field is saved and the old one is deleted.
    //! @param field - save field where the value should be stored. See enum ConfigFields. Only values from this enum are supported.
    //! @param value - unsigned value to be set in config.
    //! @warning This function is not yet implemented.
	void SetConfig(ConfigField field, uint64 value);
    //! @brief Sets given config file with signed value.
    //! @post Config field is saved and the old one is deleted.
    //! @param field - save field where the value should be stored. See enum ConfigFields. Only values from this enum are supported.
    //! @param value - unsigned value to be set in config.
    //! @warning This function is not yet implemented.
	void SetConfig(ConfigField field, int64 value);
    //! @brief Sets given config file with string value.
    //! @post Config field is saved and the old one is deleted.
    //! @param field - save field where the value should be stored. See enum ConfigFields. Only values from this enum are supported.
    //! @param value - unsigned value to be set in config.
	void SetConfig(ConfigField field, ccstring value);
    //! @brief Sets given config file with unsigned value.
    //! @post Config field is saved and the old one is deleted.
    //! @param field - save field where the value should be stored. See enum ConfigFields. Only values from this enum are supported.
    //! @param value - unsigned value to be set in config.
    //! @warning This function is not yet implemented.
	void SetConfig(ConfigField field, float64 value);


    //! @brief Checks if given config value is stored in Config.
    //! @details If field is not marked as NULLABLE in enum ConfigFields, it will always return true.
    //! @returns true if given config is stored in Config.
    //! @param field - save field where the value should be stored. See enum ConfigFields. Only values from this enum are supported.
	bool HasConfig(ConfigField field) const;

    //! @brief Initializes config database with console and config file.
    //! @details Reads all arguments from command line, and after that reads config file.
    //!     That means that the config file will override every config specified in the command line.
    //! @post Config values are read and stored.
    //! @returns true is all operations were successful.
    //! @param argc -  command line argument count.
    //! @param argv - command line array of argument strings.
    //! @param command - identifier of the command executed by the program.
	uint32 RetrieveConfig(int argc, ccstring* argv, CommandId command);

private:
    //! @brief Array of configs values.
    //! @details Values are stored as a plain strings.
	cstring* TODELETE OFDELETE OFNULLABLE configs;

    //! @brief Get internal config value.
    //! @details Lose NULLABLE is field in ConfigFields is not NULLABLE.
    //! @returns value of the config field.
    //! @param field - save field where the value is stored. See enum ConfigFields. Only values from this enum are supported.
	ccstring NULLABLE _GetConfig(ConfigField field) const;

    //! @brief Resets all config fields to default values.
    //! @post All config values are set back to default values.
	void _SetDefaults();
    //! @brief Sets all config fields to empty strings.
    //! @details Used by _SetDefault() to clear entire config memory.
    //! @post All config values are nulled.
    void _SetDefaultsNulled();
    //! @brief Sets new internal config.
    //! @details Deletes the old value of the field and sets a new one
    //! @param field - save field where the value is stored. See enum ConfigFields. Only values from this enum are supported.
    //! @param config - prepared data to be stored in an array. Will be deleted.
	void _SetNewConfig(ConfigField field, cstring TODELETE config);
};

#endif // !CONFIG_HPP