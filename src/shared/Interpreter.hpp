#ifndef INTERPRETER_H
#define INTERPRETER_H

#include "helpers.hpp"

//! @brief List of all commands available.
//! @author Monich
//! @version 1.1.0
enum CommandsIds
{
	COMMAND_INVALID = 0,
	COMMAND_HELP,
	COMMAND_ENCODE,
	COMMAND_DECODE,

	COMMAND_MAX,
};

class Config;

struct InterpreterEntry;

//! @brief Field for ConfigIds enum.
typedef uint8 CommandId;
//! @brief Field for ConfigFields enum - used for saving flags.
typedef uint8 CommandFlag;
//! @brief Typedef for main interpreter function.
typedef int(*InterpreterFn)(const int, ccstring*, Config*);
//! @brief Typedef for help interpreter function.
typedef void(*InterpreterHelpFn)();

//! @brief Interprets user input and launches main module functionalities.
//! @author Monich
//! @version 1.0.0
class Interpreter
{
public:
    //! @brief Launches main application interpreter.
    //! @returns application return code.
    //! @param argc - command line argument count.
    //! @param argv - command line arguments.
	static int Launch(int argc, ccstring* argv);

    //! @brief Launches "help" command.
    //! @details Displays help for application or specified command
    //! @returns application return code.
    //! @param argc - command line argument count.
    //! @param argv - command line arguments.
    //! @param c - current application configuration.
	static int LaunchHelp(int argc, ccstring* argv, Config* c);
    //! @brief Launches main application help.
	static void LaunchHelp_Help();

private:
    //! @brief Prepares Config and launches specified command.
    //! @returns application return code.
    //! @param command - identifier of the command to be executed.
    //! @param argc - command line argument count.
    //! @param argv - command line arguments.
	static int _PrepareCommand(CommandId command, int argc, ccstring* argv);

    //! @brief Interpretes string command to command identifier.
    //! @returns identifier of the command to be executed.
    //! @param key - Command will be read from this string.
	static const CommandId _GetCommandIdByKey(ccstring key);
    //! @brief Gets set of functions of the given command.
    //! @returns set of functions of the given command.
    //! @param id - identifier of the entry to be read.
	static const InterpreterEntry* _GetInterpreterEntry(CommandId id);
};

#endif // !INTERPRETER_H
