#include "helpers.hpp"

#include <array>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <memory>

//! @version 1.0.0
cstring TODELETE prepareCString(uint64 lenght)
{
    cstring res = new char[lenght + 1];
    res[lenght] = '\0';
    return res;
}

//! @version 1.0.1
cstring TODELETE copyCString(ccstring other)
{
#ifdef EXTENDED_CHECKS
    ASSERT(other != NULL, "copyCString: no other string");
#endif

	cstring res = prepareCString(strlen(other));
	strcpy(res, other);
	return res;
}

//! @version 1.0.2
cstring mergeCString(ccstring first, ccstring second)
{
#ifdef EXTENDED_CHECKS
    ASSERT(first != NULL, "mergeCString: first string is null");
    ASSERT(second != NULL, "mergeCString: second string is null");
#endif
    size_t strSizes[2];
    strSizes[0] = strlen(first);
    strSizes[1] = strlen(second);
    cstring res = prepareCString(strSizes[0] + strSizes[1]);
    strcpy(res, first);
    strcpy(&res[strSizes[0]], second);
    return res;
}

//! @version 1.0.0
cstring mergeCString(ccstring first, ccstring second, ccstring third)
{
#ifdef EXTENDED_CHECKS
    ASSERT(first != NULL, "mergeCString: first string is null");
    ASSERT(second != NULL, "mergeCString: second string is null");
    ASSERT(third != NULL, "mergeCString: third string is null");
#endif
    size_t strSizes[3];
    strSizes[0] = strlen(first);
    strSizes[1] = strlen(second);
    strSizes[2] = strlen(third);
    cstring res = prepareCString(strSizes[0] + strSizes[1] + strSizes[2]);
    strcpy(res, first);
    strcpy(&res[strSizes[0]], second);
    strcpy(&res[strSizes[1] + strSizes[0]], third);
    return res;
}

//! @version 1.0.0
uint64 GenerateRandom()
{
    uint64 res = 0;

    res = 0;
    for (int i = 3; i >= 0; i--)
    {
        res <<= 8;
        res += 0xFF & rand();
    }

    return res;
}

/* Not windows compatible
//! @version 1.0.0
cstring system_out(ccstring command)
{
#ifdef EXTENDED_CHECKS
    ASSERT(command, "system_out: no command to execute");
#endif
    std::array<char, 128> buffer;
    std::string result;
    std::shared_ptr<FILE> pipe(popen(command, "r"), pclose);
    if (!pipe)
        return NULL;
    while (!feof(pipe.get())) {
        if (fgets(buffer.data(), 128, pipe.get()) != nullptr)
            result += buffer.data();
    }
    return copyCString(result.c_str());
}
*/

#ifdef EXTENDED_CHECKS

//! @version 1.0.0
void ASSERT(bool statement, ccstring errString)
{
    if(!statement)
    {
        printf("ASSERT: CRITICAL ERROR ENCOUNTERED: %s\n EXITING MODULE IMMEDIATELY!", errString);
        exit(1);
    }
}

#endif // EXTENDED_CHECKS
