
# List options
option(ENVIROMENT_DEV "Application will check for extended errors. This is recommended only during development." FALSE)
option(ENABLE_TESTS "Enable tests" FALSE)

# Enable options

if(ENVIROMENT_DEV)
    message(STATUS "Building for development enviroment, extended checks enabled")
    message(STATUS "To build for production use 'cmake -DENVIROMENT_DEV=FALSE .")
    add_definitions(-DEXTENDED_CHECKS)
else(ENVIROMENT_DEV)
    message(STATUS "Building for production enviroment, no extended checks included")
    message(STATUS "To build for development use 'cmake -DENVIROMENT_DEV=TRUE .")
endif(ENVIROMENT_DEV)

if(ENABLE_TESTS)
    message(STATUS "Building tests, use 'cmake -DENABLE TESTS=FALSE' to disable")
    include(src/tests/CMakeLists.txt)
    message(STATUS "Looking for testing framework Catch2")
    find_package(Catch2 REQUIRED)
else(ENABLE_TESTS)
    message(STATUS "Skipping tests, use 'cmake -DENABLE TESTS=TRUE' to enable")
endif(ENABLE_TESTS)

