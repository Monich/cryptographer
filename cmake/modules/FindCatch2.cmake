FIND_PATH(CATCH2_DIR 
	NAMES catch2/catch.hpp
	PATHS
    deps/include
    /usr/include
  DOC "The directory containing single header Catch2."
)

if(CATCH2_DIR)
    message(STATUS "Found Catch2 include folder: ${CATCH2_DIR}")
    include_directories(${CATCH2_DIR})
    set(${CATCH2_FOUND} 1)
else(CATCH2_DIR)
    message(FATAL_ERROR "CMake didn't found Catch2 include folder, please set it manually or use install dependencies script.")
endif(CATCH2_DIR)

