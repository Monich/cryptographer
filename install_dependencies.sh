#!/bin/bash

##### Checking for dependencies

# Dependency: Git
git --version 2>&1 >/dev/null
GIT_IS_AVAILABLE=$?
if [ $GIT_IS_AVAILABLE -eq 0 ]
then
    echo "Git found on the system."
else
    echo "Git not found on the system. It is required for this script to work. Install Git and retry."
    exit 1
fi

# Dependency: CMake
cmake --version 2>&1 >/dev/null
CMAKE_IS_AVAILABLE=$?
if [ $CMAKE_IS_AVAILABLE -eq 0 ]
then
    echo "CMake found on the system."
else
    echo "CMake not found on the system. It is required for this script to work. Install CMake and retry."
    exit 1
fi

# Dependency: Catch2
if [ "$1" == "-test" ]
then
    echo "Installing Catch2 enviroment..."
    [ ! -d "deps" ] && mkdir deps
    git clone https://github.com/catchorg/Catch2.git
    cp -r Catch2/single_include deps/include
    rm -rf Catch2
    echo "Catch2 installed."
else
    echo "Testing enviroment disabled. Add -test parameter to install Catch2 locally."
fi
